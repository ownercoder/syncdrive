APP_NAME = SyncDriveService

CONFIG += qt warn_on

INCLUDEPATH += src/MyDrive

QMAKE_CXXFLAGS_DEBUG -= -g
QMAKE_CXXFLAGS_DEBUG += -g3

QMAKE_CFLAGS_RELEASE += -fPIC
QMAKE_CXXFLAGS_RELEASE += -fPIC
QMAKE_LFLAGS_RELEASE += -Wl,-z,relro -pie

QT += core network

include(config.pri)

LIBS += -lbbdata -lbbdevice -lbbsystem -lbb -lbbplatform -lsqlite3 -lc
