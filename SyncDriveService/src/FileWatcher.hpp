/*
 * FileWatcher.hpp
 *
 *  Created on: 24 мая 2016 г.
 *      Author: sergey
 */

#ifndef FILEWATCHER_HPP_
#define FILEWATCHER_HPP_

#include <QObject>
#include <QMap>
#include <QThread>
#include <sys/inotify.h>

class FileWatcher : public QObject
{
    Q_OBJECT
public:
    FileWatcher();
    virtual ~FileWatcher();
    void watchLoop();
    void addWatch(const QString &path);
    void start();
    void stop();
signals:
    void fileChanged(const QString &filepath, const QString &source);
protected:
    int fd;
    QMap<int32_t, QString> wdList;
private:
    QThread* watchThread;
};

#endif /* FILEWATCHER_HPP_ */
