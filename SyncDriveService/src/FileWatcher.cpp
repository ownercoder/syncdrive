/*
 * FileWatcher.cpp
 *
 *  Created on: 24 мая 2016 г.
 *      Author: sergey
 */

#include <src/FileWatcher.hpp>
#include <QDebug>
#include <QDirIterator>
#include <QDir>
#include <QDateTime>
#include <QCryptographicHash>
#include <QThread>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>

#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define EVENT_BUF_LEN     ( 1024 * ( EVENT_SIZE + 16 ) )

class WatchThread : public QThread {
public:
    FileWatcher* watcher;
    void run() { watcher->watchLoop(); };
};

FileWatcher::FileWatcher()
{
    fd = inotify_init();

    if ( fd < 0 ) {
        qFatal("Cannot get inotify_init");
    }
}

void FileWatcher::watchLoop()
{
    qDebug() << "Thread started";
    while(true) {
        char buffer[EVENT_BUF_LEN];
        int i = 0;
        int length = read( fd, buffer, EVENT_BUF_LEN );

        if ( length < 0 ) {
            qFatal("Error reading events");
        }

        while ( i < length ) {
            struct inotify_event *event = ( struct inotify_event * ) &buffer[ i ];
            i += EVENT_SIZE + event->len;
            if (event->len) {
                if (!wdList.contains(event->wd)) {
                    qDebug() << "Watch directory handle not exists";
                    continue;
                }

                QString path = wdList.value(event->wd);

                QString filepath = path + "/" + QString::fromUtf8(event->name);

                if (QFileInfo(filepath).size() <= 0) {
                    qDebug() << "File empty size: " << filepath;
                    continue;
                }

                emit fileChanged(filepath, path);
            }
        }
    }
}

void FileWatcher::addWatch(const QString &path)
{
    qDebug() << path.toUtf8().constData();
    int32_t wd = inotify_add_watch( fd, path.toUtf8().constData(), IN_CLOSE_WRITE );
    wdList.insert(wd, path);
}

FileWatcher::~FileWatcher()
{
    close(fd);
}

void FileWatcher::start()
{
    watchThread = new WatchThread();
    ((WatchThread*)watchThread)->watcher = this;
    watchThread->start();
}

void FileWatcher::stop()
{
    if (!watchThread) {
        return;
    }
    qDebug("Terminating watchThread");
    watchThread->terminate();
    qDebug("Waiting...");
    watchThread->wait(2000);
    qDebug("Done.");
    delete watchThread;

    QMap<int32_t, QString>::iterator i;

    for (i = wdList.begin(); i != wdList.end(); ++i)
    {
        inotify_rm_watch(fd, i.key());
    }
}
