/*
 * DriveSync.hpp
 *
 *  Created on: 21 мая 2016 г.
 *      Author: sergey
 */

#ifndef DRIVESYNC_HPP_
#define DRIVESYNC_HPP_

#include <QObject>
#include <QThread>
#include <QSettings>
#include <QFileSystemWatcher>
#include <QTimer>
#include <bb/device/HardwareInfo>
#include "GoogleSession.hpp"
#include "SessionManager.hpp"
#include "ApiWrapper.hpp"
#include "DriveSyncDatabase.hpp"
#include "FileWatcher.hpp"
#include "DirectoryStructureGet.hpp"
#include "DirectoryStructureCreate.hpp"
#include "GoogleDriveUploader.hpp"
#include "tc/utils/ConnectionInfo.hpp"

using namespace tc::utils;

class DriveSync: public QObject
{
    Q_OBJECT
public:
    DriveSync(DriveSyncDatabase *database, QObject *parent);
    void sync();
    virtual ~DriveSync() {};
public Q_SLOTS:
    void onFileChanged(const QString &path, const QString &source);
    void onUploadStarted();
    void onUploadCompleted();
    void onUploadProgress(qint64 bytesSent, qint64 bytesTotal, const QString &fileName);
    void waitForConfig();
    void onError(const QString &errorText, const SyncItem &item);
    void onSettingsUpdated(const QString &filename);
    void onConnectedChanged(bool connected);
    void onInterfaceTypeChanged(tc::utils::ConnectionInfo::Type interfaceType);
    void onSessionRefreshed();
protected:
    void updateSyncInfo(const bool progress);
    bool isSyncAllowed();
    enum SyncState {
        NotAllowed, Allowed
    };
    SessionManager * sessionManager;
    GoogleSession currentSession;
    QFileSystemWatcher *configWatcher;
    DriveSyncDatabase *p_database;
    QString rootFolderId;
    QString currentParentId;
    FileWatcher *watcher;
    SyncItem currentItem;
    GoogleDriveUploader *uploader;
    QSettings settings;
    ConnectionInfo *connectionInfo;
    QTimer configWaitTimer;
    SyncState stateSync;
    QString folderSuffix;
    static const QString BLACKBERRY_FOLDER;
    QMap<QString, QString> folderCache;
};

#endif /* DRIVESYNC_HPP_ */
