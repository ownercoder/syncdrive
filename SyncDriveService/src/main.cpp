/*
 * Copyright (c) 2013-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "service.hpp"

#include <bb/Application>

#include <QLocale>
#include <QTranslator>
#include <qmetatype.h>

#include "GoogleSession.hpp"
#include "FileMetaData.hpp"

using namespace bb;

#include <QApplication>
class MyApplication : public QApplication {
public:
  MyApplication(int& argc, char ** argv) :
    QApplication(argc, argv) { }
  virtual ~MyApplication() { }

  virtual bool notify(QObject * receiver, QEvent * event) {
    try {
      return QApplication::notify(receiver, event);
    } catch(std::exception& e) {
      qCritical() << "Exception thrown:" << e.what();
      this->quit();
    }
    return false;
  }
};


int main(int argc, char **argv)
{
    MyApplication app(argc, argv);

    qRegisterMetaTypeStreamOperators<GoogleSession>("GoogleSession");
    qRegisterMetaTypeStreamOperators<QList<GoogleSession> >("QList<GoogleSession>");
    qRegisterMetaType< QList<FileMetaData> >("QList<FileMetaData>");

    QCoreApplication::setOrganizationName("Slytech");
    QCoreApplication::setOrganizationDomain("slytech.com");
    QCoreApplication::setApplicationName("SyncDrive");

    // Create the Application UI object, this is where the main.qml file
    // is loaded and the application scene is set.
    Service srv;

    // Enter the application main event loop.
    return MyApplication::exec();
}
