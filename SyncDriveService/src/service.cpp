/*
 * Copyright (c) 2013-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "service.hpp"

#include <QDebug>
#include <bb/Application>
#include <bb/platform/Notification>
#include <bb/platform/NotificationDefaultApplicationSettings>
#include <bb/system/InvokeManager>

#include <QTimer>

#include "DriveSync.hpp"
#include "DriveSyncDatabase.hpp"
#include "FileWatcher.hpp"

using namespace bb::platform;
using namespace bb::system;

Service::Service() :
        QObject(),
        m_invokeManager(new InvokeManager(this))
{
    m_invokeManager->connect(m_invokeManager, SIGNAL(invoked(const bb::system::InvokeRequest&)),
            this, SLOT(handleInvoke(const bb::system::InvokeRequest&)));

    database = new DriveSyncDatabase(this);
    database->openDatabase(QDir::currentPath() + "/data/DriveSyncQueue.db");

    driveSync = new DriveSync(database, this);
}

void Service::handleInvoke(const bb::system::InvokeRequest & request)
{

}

void Service::onQueueUpdated()
{

}
