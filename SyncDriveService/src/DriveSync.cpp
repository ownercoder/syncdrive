/*
 * DriveSync.cpp
 *
 *  Created on: 21 мая 2016 г.
 *      Author: sergey
 */

#include <src/DriveSync.hpp>

#include <QDebug>
#include <bb/device/SdCardInfo>

#include "SessionManager.hpp"
#include "TokenManager.hpp"
#include "GoogleFiles.hpp"
#include "SessionWrapper.hpp"
#include "GoogleDriveUploader.hpp"
#include "SyncedItem.hpp"
#include "SyncItem.hpp"
#include <QDir>
#include <QFileInfo>
#include <QRegExp>
#include <QDateTime>

const QString DriveSync::BLACKBERRY_FOLDER = "BlackBerry-";

DriveSync::DriveSync(DriveSyncDatabase *database, QObject *parent)
    : QObject(parent), p_database(database)
{
    folderSuffix = bb::device::HardwareInfo().pin().right(8);
    TokenManager *tokenManager = new TokenManager(this);
    sessionManager = new SessionManager(this);
    sessionManager->setTokenManager(tokenManager);
    connect(sessionManager, SIGNAL(sessionRefreshCompleted()), this, SLOT(onSessionRefreshed()));

    currentSession = sessionManager->getCurrentSession();

    watcher = new FileWatcher();
    connect(watcher, SIGNAL(fileChanged(QString, QString)), this, SLOT(onFileChanged(QString, QString)));

    uploader = new GoogleDriveUploader(this);
    uploader->setSessionManager(sessionManager);
    connect(uploader, SIGNAL(uploadStarted()), this, SLOT(onUploadStarted()));
    connect(uploader, SIGNAL(uploadCompleted()), this, SLOT(onUploadCompleted()));
    connect(uploader, SIGNAL(uploadProgress(qint64, qint64, const QString &)), this, SLOT(onUploadProgress(qint64, qint64, const QString &)));
    connect(uploader, SIGNAL(error(QString, SyncItem)), this, SLOT(onError(QString, SyncItem)));

    connectionInfo = new ConnectionInfo();
    connect(connectionInfo, SIGNAL(connectedChanged(bool)), this, SLOT(onConnectedChanged(bool)));
    connect(connectionInfo, SIGNAL(interfaceTypeChanged(tc::utils::ConnectionInfo::Type)), this, SLOT(onInterfaceTypeChanged(tc::utils::ConnectionInfo::Type)));

    configWatcher = new QFileSystemWatcher(this);
    connect(configWatcher, SIGNAL(fileChanged(QString)), this, SLOT(onSettingsUpdated(QString)));

    waitForConfig();

    stateSync = DriveSync::Allowed;
}

void DriveSync::onFileChanged(const QString &path, const QString &source)
{
    qDebug() << "File changed " << path;

    if (!p_database->containsInQueue(path)) {
        p_database->addToQueue(path, source);
        sync();
    }
}

void DriveSync::waitForConfig()
{
    if ( ! QFileInfo(settings.fileName()).isReadable() ) {
        qDebug() << "No config file: " << settings.fileName() << " wait for 5 seconds";
        QTimer::singleShot(5000, this, SLOT(waitForConfig()));
        return;
    }
    configWatcher->addPath(settings.fileName());
    settings.setValue("time", QDateTime::currentDateTime().toTime_t());
    settings.sync();
}

void DriveSync::onSessionRefreshed()
{
    sync();
}

bool DriveSync::isSyncAllowed()
{
    if (stateSync == DriveSync::NotAllowed) {
        return false;
    }

    bool connected = connectionInfo->isConnected();

    tc::utils::ConnectionInfo::Type interfaceType = connectionInfo->interfaceType();

    qDebug() << "Connection type " << interfaceType << " connected: " << connected;

    if (!connected)
    {
        return false;
    }

    if (interfaceType == ConnectionInfo::Cellular && !settings.value("cellularEnabled", false).toBool())
    {
        return false;
    }

    return true;
}

void DriveSync::onInterfaceTypeChanged(tc::utils::ConnectionInfo::Type interfaceType)
{
    Q_UNUSED(interfaceType);

    sync();
}

void DriveSync::onConnectedChanged(bool connected)
{
    Q_UNUSED(connected);

    sync();
}

void DriveSync::onSettingsUpdated(const QString &filename)
{
    Q_UNUSED(filename);

    settings.sync();

    qDebug() << "Config changed, start update";

    watcher->stop();

    bb::device::SdCardInfo sdInfo;

    if (settings.value("syncPhotos", false).toBool()) {
        if (sdInfo.state() == 1) {
            watcher->addWatch("/accounts/1000/removable/sdcard/camera");
        }
        watcher->addWatch("/accounts/1000/shared/camera");
        qDebug() << "Add watch photos";
    }

    if (settings.value("syncVideos", false).toBool()) {
        if (sdInfo.state() == 1) {
            watcher->addWatch("/accounts/1000/removable/sdcard/videos");
        }
        watcher->addWatch("/accounts/1000/shared/videos");
        qDebug() << "Add watch videos";
    }

    settings.beginGroup("Folders");
    QStringList keys = settings.allKeys();
    for(int i = 0;i < keys.length(); i++) {
        qDebug() << "Add folder for watch: " << "/" + keys.at(i);
        watcher->addWatch("/" + keys.at(i));
    }
    settings.endGroup();

    watcher->start();

    sync();
}

void DriveSync::sync()
{
    if (!p_database->queueLength()) {
        return;
    }

    if (!this->isSyncAllowed()) {
        return;
    }

    if (sessionManager->isSessionExpired()) {
        sessionManager->refreshExpiredSessions();
        return;
    }

    stateSync = DriveSync::NotAllowed;

    updateSyncInfo(true);

    for(int i = 0; i < p_database->queueLength(); i++) {
        currentItem = p_database->shift();

        QFileInfo fi(currentItem.source);
        QString baseName = fi.baseName();

        QString canonicalPath = QFileInfo(currentItem.file).absolutePath();
        canonicalPath.replace(currentItem.source, "");
        canonicalPath.replace(QRegExp("(/$|^/)"), "");
        QString destPath = DriveSync::BLACKBERRY_FOLDER + folderSuffix + "/" + baseName + "/" + canonicalPath;
        QString destFolderId;

        if (!folderCache.contains(destPath)) {
            DirectoryStructureCreate *directoryStructureCreate = new DirectoryStructureCreate();
            destFolderId = directoryStructureCreate->createPath(destPath);

            folderCache.insert(destPath, destFolderId);

            delete directoryStructureCreate;
        } else {
            destFolderId = folderCache.value(destPath);
        }

        uploader->addToQueue(currentItem, destFolderId);
    }

    uploader->upload();
}

void DriveSync::onUploadStarted()
{
    stateSync = DriveSync::NotAllowed;
    updateSyncInfo(true);
    qDebug() << "Uploading started";
}

void DriveSync::onUploadCompleted()
{
    stateSync = DriveSync::Allowed;
    updateSyncInfo(false);
    //currentItem.checksum = watcher->getHash(currentItem.file);
    //p_database->addSynced(currentItem);
    sync();

    qDebug() << "Uploading ended";
}

void DriveSync::onUploadProgress(qint64 bytesSent, qint64 bytesTotal, const QString &fileName)
{
    qDebug() << "Uploading " << bytesSent << "/" << bytesTotal << " " << fileName;
}

void DriveSync::onError(const QString &errorText, const SyncItem &item)
{
    stateSync = DriveSync::Allowed;
    p_database->unShift(item);
    updateSyncInfo(false);
    if (errorText == "203") {
        folderCache.clear();
        sync();
    }

    qDebug() << "Error uploading " << errorText << " file: " << item.file;
}

void DriveSync::updateSyncInfo(const bool progress)
{
    QFile *file = new QFile("data/lastsync");
    if (!file->open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        qDebug() << file->errorString();
        return;
    }

    QDataStream data(file);
    data << QDateTime::currentDateTime().toTime_t();
    data << progress;

    file->close();
    delete file;
}

