/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/Sheet>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeRequest>
#include <bb/system/InvokeTargetReply>

#include "support/qmlbeam.h"
#include "SessionManager.hpp"
#include "TokenManager.hpp"
#include "GoogleFiles.hpp"
#include "WebImageView.hpp"
#include "SearchDataModel.hpp"
#include "AccountDataModel.hpp"
#include "SessionWrapper.hpp"
#include "ApiWrapper.hpp"
#include "GoogleDriveUploader.hpp"
#include "UserInfo.hpp"
#include "Settings.hpp"
#include "ConnectionInfo.hpp"

using namespace bb::cascades;

ApplicationUI::ApplicationUI(QObject * parent) :
        QObject(parent)
        , m_invokeManager(new bb::system::InvokeManager(this)) {
    // prepare the localization
    m_pTranslator = new QTranslator(this);
    m_pLocaleHandler = new LocaleHandler(this);

    userInfo = new UserInfo(this);
    connect(userInfo, SIGNAL(infoReceived(QList<UserInfoData>)), this, SLOT(onInfoReceived(QList<UserInfoData>)));

    bool res = QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()));
    // This is only available in Debug builds
    Q_ASSERT(res);
    // Since the variable is not used in the app, this is added to avoid a
    // compiler warning
    Q_UNUSED(res);

    // initial load
    onSystemLanguageChanged();

    qRegisterMetaType< QList<FileMetaData> >("QList<FileMetaData>");
    qRegisterMetaType< UserInfoData >("UserInfoData");

    //qmlRegisterType<FilesDataModel>("com.slytech.filesdatamodel", 1, 0, "FilesDataModel");
    qmlRegisterType<GoogleFiles>("com.slytech.googlefiles", 1, 0, "GoogleFiles");
    qmlRegisterType<GoogleFiles>("com.slytech.googlefiles", 1, 0, "GoogleFiles");
    qmlRegisterType<SearchDataModel>("com.slytech.searchdatamodel",1, 0, "SearchDataModel");
    qmlRegisterType<AccountDataModel>("com.slytech.accountdatamodel",1, 0, "AccountDataModel");
    qmlRegisterType<ConnectionInfo>("com.slytech.connectioninfo",1, 0, "ConnectionInfo");
    qmlRegisterType<SessionWrapper>("com.slytech.sessionwrapper",1, 0, "SessionWrapper");
    qmlRegisterType<ApiWrapper>("com.slytech.apiwrapper",1, 0, "ApiWrapper");
    qmlRegisterType<GoogleDriveUploader>("com.slytech.googleuploader", 1, 0, "GoogleDriveUploader");
    qmlRegisterType<Settings>("com.slytech.settings", 1, 0, "Settings");
    qmlRegisterType<WebImageView>("my.library",1, 0, "WebImageView");

    TokenManager * tokenManager = new TokenManager();
    connect(tokenManager, SIGNAL(sessionCreated(GoogleSession)), this, SLOT(onSessionCreated(GoogleSession)));

    sessionManager = new SessionManager();
    sessionManager->setTokenManager(tokenManager);

    showMainApp();

    //updateUserInfo();

    new QmlBeam(this);

    startSyncDriveService();

    watchForLastSync();
}

void ApplicationUI::updateUserInfo()
{
    if (sessionManager->getCurrentSession().token.length()) {
        QList<GoogleSession> sessions;
        sessions << sessionManager->getCurrentSession();
        userInfo->getUserDataInfo(sessions);
    }
}


void ApplicationUI::processToken(const QString &code)
{
    sessionManager->getToken(code);
    QMetaObject::invokeMethod(root, "closeIntroView");
}

void ApplicationUI::watchForLastSync()
{
    QString watchFilepath = QDir::currentPath() + "/data/lastsync";
    if (!QFileInfo(watchFilepath).isReadable()) {
        QFile file(watchFilepath);
        file.open(QIODevice::WriteOnly);
        file.close();
    }

    QFileSystemWatcher *lastSyncWatcher = new QFileSystemWatcher(this);
    connect(lastSyncWatcher, SIGNAL(fileChanged(QString)), this, SLOT(onLastSyncUpdated(QString)));
    lastSyncWatcher->addPath(watchFilepath);

    onLastSyncUpdated(watchFilepath);
}

void ApplicationUI::onInfoReceived(QList<UserInfoData> infos)
{
    if (infos.length() > 0) {
        userInfoData = infos.at(0);
    }

    QMetaObject::invokeMethod(root, "updateUserInfo", Q_ARG(QVariant, userInfoData.toMap()));
}

void ApplicationUI::onLastSyncUpdated(const QString &filename)
{
    Q_UNUSED(filename)

    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Cannot open file: data/lastsync " << file.errorString();
        return;
    }

    uint lastsync;
    bool inProgress;
    QDataStream data(&file);
    data >> lastsync;
    data >> inProgress;

    file.close();

    QMetaObject::invokeMethod(root, "updateLastSync", Q_ARG(QVariant, lastsync), Q_ARG(QVariant, inProgress));
}

QString ApplicationUI::getLoginUrl() {
    return sessionManager->getAuthenticationUrl(SessionManager::Drive | SessionManager::Profile | SessionManager::Email).toString();
}

void ApplicationUI::changeAccount(const QString &id) {
    QList<GoogleSession> sessionList = sessionManager->getSessions();
    for(int i=0;i<sessionList.length();i++) {
        if (sessionList.at(i).uniqId == id) {
            sessionManager->setCurrentSession(sessionList.at(i));
            return;
        }
    }

    qDebug() << "Cannot find session id:" << id;
    return;
}

void ApplicationUI::onSessionCreated(GoogleSession session) {
    qDebug() << "Session created: " << session.token;
    sessionManager->storeSession(session);
    sessionManager->setCurrentSession(session);
    QList<GoogleSession> sessions;
    sessions << session;
    userInfo->getUserDataInfo(sessions);
}

void ApplicationUI::onSystemLanguageChanged() {
    QCoreApplication::instance()->removeTranslator(m_pTranslator);
    // Initiate, load and install the application translation files.
    QString locale_string = QLocale().name();
    QString file_name = QString("MyDrive_%1").arg(locale_string);
    if (m_pTranslator->load(file_name, "app/native/qm")) {
        QCoreApplication::instance()->installTranslator(m_pTranslator);
    }
}

void ApplicationUI::showLoginWebView()
{
    QmlDocument *qml = QmlDocument::create("asset:///LoginWebViewSheet.qml").parent(this);

    qml->setContextProperty("app", this);

    // Create root object for the UI
    loginWebViewSheet = qml->createRootObject<Sheet>();
    loginWebViewSheet->open();
}

void ApplicationUI::showMainApp() {
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    qml->setContextProperty("app", this);

    // Create root object for the UI
    root = qml->createRootObject<AbstractPane>();

    Application::instance()->setScene(root);
}

void ApplicationUI::startSyncDriveService() {
    qDebug() << "requesting to start SyncDriveService";
    bb::system::InvokeRequest request;
    request.setTarget("com.slytech.SyncDriveService");
    request.setAction("bb.action.START");
    request.setMimeType("text/plain");
    bb::system::InvokeTargetReply *reply = m_invokeManager->invoke(request);
    if (!reply) {
        qDebug() << "failed to start SyncDriveService: " << reply->errorCode();
        reply->deleteLater();
    }
}
