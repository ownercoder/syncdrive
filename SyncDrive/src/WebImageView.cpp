#include <QNetworkReply>
#include <QNetworkDiskCache>
#include <bb/cascades/Application>
#include <bb/cascades/Image>
#include <bb/ImageData>
#include <bb/cascades/Image>
#include <bb/utility/ImageConverter>

#include "WebImageView.hpp"

using namespace bb::cascades;

QNetworkAccessManager * WebImageView::mNetManager = new QNetworkAccessManager(Application::instance());
QNetworkDiskCache * WebImageView::mNetworkDiskCache = new QNetworkDiskCache();

WebImageView::WebImageView(Container * parent) : bb::cascades::ImageView(parent) {
    // Initialize network cache
    QDir cacheDir("data/cache/");
    if (!cacheDir.exists()) {
        cacheDir.mkdir(cacheDir.dirName());
    }
    mNetworkDiskCache->setCacheDirectory(cacheDir.absolutePath());

    // Set cache in manager
    mNetManager->setCache(mNetworkDiskCache);

    // Set defaults
    mLoading = 0;

    connect(this, SIGNAL(urlChanged()), this, SLOT(onUrlChanged()));
}

const QUrl& WebImageView::imageMask() const {
    return maskImageUrl;
}

void WebImageView::setImageMask(const QUrl &url) {
    maskImageUrl = url;
    QString filePath = QDir::currentPath() + "/app/native/assets/" + url.toString(QUrl::RemoveScheme).replace("///", "");
    QFile file(filePath);
    if (file.open(QFile::ReadOnly)) {
        maskImageData = file.readAll();
        setupImageMask();
    }
    file.close();
}

void WebImageView::setupImageMask()
{
    if (imageData.size() == 0 || maskImageData.size() == 0) {
        return;
    }

    QImage origQImage = QImage::fromData(imageData);
    QImage shapeQImage = QImage::fromData(maskImageData);
    bb::ImageData imageDataRaw(bb::PixelFormat::RGBA_Premultiplied, origQImage.width(), origQImage.height());
    int y = 0;

    unsigned char *dstLine = imageDataRaw.pixels();

    for (y = 0; y < origQImage.height(); y++)
    {
        unsigned char * dst = dstLine;
        for (int x = 0; x < imageDataRaw.width(); x++)
        {
            QRgb imagePixel = origQImage.pixel(x, y);
            QRgb shapePixel = shapeQImage.pixel(x, y);

            *dst++ = qRed(imagePixel) * qAlpha(shapePixel) / 255;
            *dst++ = qGreen(imagePixel) * qAlpha(shapePixel) / 255;
            *dst++ = qBlue(imagePixel) * qAlpha(shapePixel) / 255;
            *dst++ = qAlpha(shapePixel);
        }
        dstLine += imageDataRaw.bytesPerLine();
    }

    setImage(Image(imageDataRaw));
}

const QUrl& WebImageView::url() const {
    return mUrl;
}

void WebImageView::setUrl(const QUrl& url) {
    // Variables
    mUrl = url;
    mLoading = 0;

    // Reset the image
    resetImage();

    if (url.isLocalFile() || url.scheme() == "asset") {
        QString filePath = QDir::currentPath() + "/app/native/assets/" + url.toString(QUrl::RemoveScheme).replace("///", "");
        QFile file(filePath);
        if (file.open(QFile::ReadOnly)) {
            imageData = file.readAll();
        }
        file.close();
        setImageSource(url);
        emit urlChanged();
        return;
    }

    if (mimeImageUrl.isValid() && !mimeImageUrl.isEmpty()) {
        setImageSource(mimeImageUrl);
    }

    if (url.isValid() && !url.isEmpty()) {
        // Create request
        QNetworkRequest request;
        request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
        request.setUrl(url);


        // Create reply
        QNetworkReply * reply = mNetManager->get(request);

        // Connect to signals
        QObject::connect(reply, SIGNAL(finished()), this, SLOT(imageLoaded()));
        QObject::connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(dowloadProgressed(qint64,qint64)));
    }

    emit urlChanged();
}

void WebImageView::onUrlChanged()
{
    setupImageMask();
}

double WebImageView::loading() const {
    return mLoading;
}

void WebImageView::imageLoaded() {
    // Get reply
    QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());

    if (reply->error() == QNetworkReply::NoError) {
        if (isARedirectedUrl(reply)) {
            setURLToRedirectedUrl(reply);
            return;
        } else {
            imageData = reply->readAll();
            setImage(Image(imageData));
        }
    }

    setupImageMask();

    // Memory management
    reply->deleteLater();
}

bool WebImageView::isARedirectedUrl(QNetworkReply *reply) {
    QUrl redirection = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
    return !redirection.isEmpty();
}

void WebImageView::setMimeImage(const QUrl& url) {
    mimeImageUrl = url;
}

const QUrl& WebImageView::mimeImage() const {
    return mimeImageUrl;
}

void WebImageView::setURLToRedirectedUrl(QNetworkReply *reply) {
    QUrl redirectionUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
    QUrl baseUrl = reply->url();
    QUrl resolvedUrl = baseUrl.resolved(redirectionUrl);

    setUrl(resolvedUrl.toString());
}

void WebImageView::clearCache() {
    mNetworkDiskCache->clear();
}

void WebImageView::dowloadProgressed(qint64 bytes, qint64 total) {
    mLoading = double(bytes) / double(total);

    emit loadingChanged();
}
