/*
 * Copyright (c) 2013-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"
#include "Settings.hpp"

#include <bb/cascades/Application>

#include <QLocale>
#include <QTranslator>

#include <Qt/qdeclarativedebug.h>

using namespace bb::cascades;

Q_DECL_EXPORT int main(int argc, char **argv)
{
    Application app(argc, argv);

    qRegisterMetaTypeStreamOperators<GoogleSession>("GoogleSession");
    qRegisterMetaTypeStreamOperators<QList<GoogleSession> >("QList<GoogleSession>");

    QCoreApplication::setOrganizationName("Slytech");
    QCoreApplication::setOrganizationDomain("slytech.com");
    QCoreApplication::setApplicationName("SyncDrive");

    // Default settings if not set
    Settings settings;
    if (!settings.contains("cellularEnabled"))
    {
        settings.setValue("cellularEnabled", false);
    }

    if (!settings.contains("syncVideos"))
    {
        settings.setValue("syncVideos", true);
    }

    if (!settings.contains("syncPhotos"))
    {
        settings.setValue("syncPhotos", true);
    }

    settings.sync();

    // Create the Application UI object, this is where the main.qml file
    // is loaded and the application scene is set.
    ApplicationUI appui;

    // Enter the application main event loop.
    return Application::exec();
}
