/*
 * DriveSyncDatabase.hpp
 *
 *  Created on: 23 мая 2016 г.
 *      Author: sergey
 */

#ifndef DRIVESYNCDATABASE_HPP_
#define DRIVESYNCDATABASE_HPP_

#include <QObject>
#include <sqlite3.h>
#include "SyncedItem.hpp"
#include "SyncItem.hpp"

class DriveSyncDatabase : public QObject
{
    Q_OBJECT
protected:
    sqlite3 *_db;

    sqlite3_stmt *_insert;
    sqlite3_stmt *_delete;

    long _max_persisted_sessions;
    long _sessions_expire_after;

    static QString StringFromUnsignedChar( const unsigned char *str );
public:
    DriveSyncDatabase(QObject *parent);
    virtual ~DriveSyncDatabase();
    bool isFileSynced(const QString &path, const QString &hash);
    void close();
    void openDatabase(const QString &path);
    void addToQueue(const QString &file, const QString &source);
    void addSynced(SyncItem item);
    void deleteSyncedItem(const SyncedItem &item);
    bool containsInQueue(const QString &file);
    SyncedItemList getSynced(const int offset = 0, const int limit = 50);
    SyncItem shift();
    void unShift(const SyncItem syncItem);
    int queueLength();
    int syncedLength();
};

#endif /* DRIVESYNCDATABASE_HPP_ */
