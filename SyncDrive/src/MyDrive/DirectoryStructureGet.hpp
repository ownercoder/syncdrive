/*
 * DirectoryStructure.hpp
 *
 *  Created on: 28 мая 2016 г.
 *      Author: sergey
 */

#ifndef DIRECTORYSTRUCTUREGET_HPP_
#define DIRECTORYSTRUCTUREGET_HPP_

#include <QObject>
#include "ApiWrapper.hpp"
#include "GoogleFiles.hpp"
#include <QQueue>
#include <QEventLoop>

class DirectoryStructureGet: public QObject
{
    Q_OBJECT
public:
    DirectoryStructureGet();
    ~DirectoryStructureGet();

    QString getIdByPath(const QString &path);
protected Q_SLOTS:
    void onGetCompleted(QList<FileMetaData> const files, const QString &nextPageToken);
    void directoryGetProcessQueue();
private:
    TokenManager *tokenManager;
    GoogleFiles *googleFiles;
    SessionManager *manager;
    QString currentFolderId;
    QQueue<QString> _queue;
    QString currentParentFolder;
    QEventLoop loop;
};

#endif /* DIRECTORYSTRUCTUREGET_HPP_ */
