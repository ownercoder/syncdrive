/*
 * GoogleDriveUploader.hpp
 *
 *  Created on: 13 июня 2015 г.
 *      Author: Sergey
 */

#ifndef GOOGLEDRIVEUPLOADER_HPP_
#define GOOGLEDRIVEUPLOADER_HPP_

#include <QQueue>
#include <QFile>
#include <QNetworkReply>
#include "DriveSyncDatabase.hpp"
#include "SyncItem.hpp"
#include "RequestManager.hpp"

class GoogleDriveUploader: public QObject
{
    Q_OBJECT
public:
    GoogleDriveUploader(QObject * parent = 0);
    ~GoogleDriveUploader();

    Q_INVOKABLE void upload();
    Q_INVOKABLE void addToQueue(const SyncItem& item, const QString &uploadTo);
    Q_INVOKABLE void abort();
    Q_INVOKABLE void setSessionManager(SessionManager * sessionManager);

Q_SIGNALS:
    void uploadStarted();
    void uploadCompleted();
    void uploadProgress(qint64 bytesSent, qint64 bytesTotal, const QString &fileName);
    void error(const QString &errorText, const SyncItem &item);
protected Q_SLOTS:
    void onUploadProgress(qint64 bytesSent, qint64 bytesTotal);
    void onFinished();
    void onReplyError(QNetworkReply::NetworkError code);
    void onSslErrors(const QList<QSslError> & errors);
    void exec();
    void onAccessManagerFinished(QNetworkReply *reply);
private:
    SessionManager *sessManager;
    QHttpMultiPart *multiPart;
    QNetworkAccessManager *accessManager;
    QFile *file;
    qint64 totalBytesForSend;
    qint64 totalBytesSent;
    QQueue< QPair<SyncItem, QString> > _queue;
};

#endif /* GOOGLEDRIVEUPLOADER_HPP_ */
