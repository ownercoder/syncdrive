/*
 * DirectoryStructure.hpp
 *
 *  Created on: 28 мая 2016 г.
 *      Author: sergey
 */

#ifndef DIRECTORYSTRUCTURECREATE_HPP_
#define DIRECTORYSTRUCTURECREATE_HPP_

#include <QObject>
#include "ApiWrapper.hpp"
#include "GoogleFiles.hpp"
#include <QQueue>
#include <QEventLoop>
#include "DirectoryStructureGet.hpp"

class DirectoryStructureCreate: public QObject
{
    Q_OBJECT
public:
    DirectoryStructureCreate();
    ~DirectoryStructureCreate();

    Q_INVOKABLE QString createPath(const QString &path);
protected Q_SLOTS:
    void onFolderCreated(const QVariantMap &data);
    void directoryCreateProcessQueue();
private:
    TokenManager *tokenManager;
    ApiWrapper *apiWrapper;
    SessionManager *manager;
    QString currentFolderId;
    QString currentPath;
    QQueue<QString> _queue;
    QEventLoop loop;
    DirectoryStructureGet *directoryStructureGet;
};

#endif /* DIRECTORYSTRUCTURECREATE_HPP_ */
