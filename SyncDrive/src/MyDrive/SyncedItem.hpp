/*
 * SyncedItem.h
 *
 *  Created on: 16 дек. 2016 г.
 *      Author: sergey
 */

#ifndef SYNCEDITEM_HPP_
#define SYNCEDITEM_HPP_

#include <QObject>

class SyncedItem
{
public:
    SyncedItem();
    virtual ~SyncedItem();
    int id;
    QString file;
    QString checksum;
};

typedef QList<SyncedItem> SyncedItemList;

#endif /* SYNCEDITEM_HPP_ */
