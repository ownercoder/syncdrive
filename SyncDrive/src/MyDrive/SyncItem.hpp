/*
 * SyncIntem.hpp
 *
 *  Created on: 27 дек. 2016 г.
 *      Author: sergey
 */

#ifndef SYNCITEM_HPP_
#define SYNCITEM_HPP_

#include <QObject>
#include <QMetaType>

typedef struct
{
    QString file;
    QString source;
    QString checksum;
} SyncItem;

Q_DECLARE_METATYPE(SyncItem);

#endif /* SYNCITEM_HPP_ */

