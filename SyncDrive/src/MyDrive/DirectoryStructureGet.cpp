/*
 * DirectoryStructureGet.cpp
 *
 *  Created on: 28 мая 2016 г.
 *      Author: sergey
 */

#include <QDebug>
#include <QTimer>
#include "DirectoryStructureGet.hpp"
#include "GoogleFiles.hpp"
#include <QFileInfo>
#include <QStringList>

DirectoryStructureGet::DirectoryStructureGet()
{
    tokenManager = new TokenManager(this);

    manager = new SessionManager(this);
    manager->setTokenManager(tokenManager);

    googleFiles = new GoogleFiles(manager, this);
    connect(googleFiles, SIGNAL(getCompleted(QList<FileMetaData> const, const QString&)), this, SLOT(onGetCompleted(QList<FileMetaData> const, const QString&)));
}

DirectoryStructureGet::~DirectoryStructureGet()
{
    delete googleFiles;
    delete manager;
    delete tokenManager;

    loop.deleteLater();
}

QString DirectoryStructureGet::getIdByPath(const QString &path)
{
    currentParentFolder = "";
    currentFolderId = "";

    QStringList fragments = path.split("/");
    for(int i=0;i < fragments.length();i++)
    {
        if (fragments.at(i).length()) _queue.enqueue(fragments.at(i));
    }

    qDebug() << "Start find path: " << path;
    if (!_queue.length()) {
        qDebug() << "Empty queue for get folder path " << path;
        return currentFolderId;
    }

    QTimer timer;
    timer.setInterval(60000);
    timer.setSingleShot(true);

    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    QTimer::singleShot(0, this, SLOT(directoryGetProcessQueue()));

    loop.exec(QEventLoop::AllEvents|QEventLoop::WaitForMoreEvents);

    qDebug() << "Path found: " << currentFolderId;
    return currentFolderId;
}

void DirectoryStructureGet::directoryGetProcessQueue()
{
    QString folder = _queue.dequeue();
    QString parent = currentParentFolder.isEmpty() ? "root" : currentParentFolder;

    googleFiles->get(folder, parent, GoogleFiles::MyFolder);
}

void DirectoryStructureGet::onGetCompleted(QList<FileMetaData> const files, const QString &nextPageToken)
{
    Q_UNUSED(nextPageToken);

    if (files.length() == 0)
    {
        currentFolderId = "";
        loop.quit();
        return;
    }

    if (_queue.length())
    {
        currentParentFolder = files.at(0).id;
        directoryGetProcessQueue();
        return;
    }

    if (files.length()) {
        currentFolderId = files.at(0).id;
    } else {
        currentFolderId = "";
    }

    loop.quit();
}

