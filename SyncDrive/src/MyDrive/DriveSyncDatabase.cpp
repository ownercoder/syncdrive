/*
 * DriveSyncDatabase.cpp
 *
 *  Created on: 23 мая 2016 г.
 *      Author: sergey
 */
#include <QDebug>
#include <DriveSyncDatabase.hpp>
#include "SyncItem.hpp"

DriveSyncDatabase::DriveSyncDatabase(QObject *parent)
    : QObject(parent)
{
}

DriveSyncDatabase::~DriveSyncDatabase()
{
    if (NULL!=_db) {
        sqlite3_finalize(_insert);
        sqlite3_finalize(_delete);
        sqlite3_close_v2(_db);
    }
}

void DriveSyncDatabase::addSynced(SyncItem item)
{
    sqlite3_stmt *insertSynced = NULL;
    int res = sqlite3_prepare_v2(_db, "insert into drivesync_synced(file, checksum) values(?1, ?2);", -1, &insertSynced, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Failed to prepare insert";
        sqlite3_finalize(insertSynced);
        return;
    }

    res = sqlite3_bind_text(insertSynced, 1, item.file.toUtf8().constData(), -1, SQLITE_TRANSIENT);
    if (SQLITE_OK != res) {
        qWarning() << "Error on Insert binding file path " << item.file << ": " << sqlite3_errmsg(_db);
        sqlite3_finalize(insertSynced);
        return;
    }

    res = sqlite3_bind_text(insertSynced, 2, item.checksum.toUtf8().constData(), -1, SQLITE_TRANSIENT);
    if (SQLITE_OK != res) {
        qWarning() << "Error on Insert binding checksum " << item.checksum << ": " << sqlite3_errmsg(_db);
        sqlite3_finalize(insertSynced);
        return;
    }

    if (SQLITE_DONE != sqlite3_step(insertSynced)) {
        qWarning() << "Error insert row " << sqlite3_errmsg(_db);
        sqlite3_finalize(insertSynced);
        return;
    }

    sqlite3_reset(insertSynced);

    sqlite3_finalize(insertSynced);
}

QString DriveSyncDatabase::StringFromUnsignedChar( const unsigned char *str )
{
    return QString::fromUtf8(reinterpret_cast<const char*>(str));
}

bool DriveSyncDatabase::containsInQueue(const QString &file)
{
    sqlite3_stmt *fetch = NULL;
    int res = sqlite3_prepare_v2(_db, "select id, file, source from drivesync_queue where file = ?1 limit 1", -1, &fetch, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Failed to prepare select for fetch";
        sqlite3_finalize(fetch);
        return false;
    }

    res = sqlite3_bind_text(fetch, 1, file.toUtf8().constData(), -1, SQLITE_TRANSIENT);
    if (SQLITE_OK != res) {
        qWarning() << "Error on binding file " << file << ": " << sqlite3_errmsg(_db);
        return false;
    }

    if (SQLITE_ROW != sqlite3_step(fetch)) {
        qWarning() << "Error retrieving row " << sqlite3_errmsg(_db);
        sqlite3_finalize(fetch);
        return false;
    }

    bool result = false;
    if (sqlite3_data_count(fetch) > 0) {
        result = true;
    } else {
        result = false;
    }

    sqlite3_finalize(fetch);

    return result;
}

void DriveSyncDatabase::addToQueue(const QString &file, const QString &source)
{
    int res = sqlite3_bind_text(_insert, 1, file.toUtf8().constData(), -1, SQLITE_TRANSIENT);
    if (SQLITE_OK != res) {
        qWarning() << "Error on Insert binding url " << file << ": " << sqlite3_errmsg(_db);
        return;
    }

    res = sqlite3_bind_text(_insert, 2, source.toUtf8().constData(), -1, SQLITE_TRANSIENT);
    if (SQLITE_OK != res) {
        qWarning() << "Error on Insert binding source path " << file << ": " << sqlite3_errmsg(_db);
        return;
    }

    res = sqlite3_step(_insert);
    if (SQLITE_DONE != res) {
        qWarning() << "Error executing insert " << file << ": " << sqlite3_errmsg(_db);
    }

    sqlite3_reset(_insert);
}

int DriveSyncDatabase::queueLength()
{
    sqlite3_stmt *fetch = NULL;
    int res = sqlite3_prepare_v2(_db, "select COUNT(id) from drivesync_queue", -1, &fetch, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Failed to prepare select for fetch";
        sqlite3_finalize(fetch);
        return 0;
    }

    if (SQLITE_ROW != sqlite3_step(fetch)) {
        qWarning() << "Error retrieving row " << sqlite3_errmsg(_db);
        sqlite3_finalize(fetch);
        return 0;
    }

    int count = sqlite3_column_int64(fetch, 0);

    sqlite3_finalize(fetch);

    return count;
}

int DriveSyncDatabase::syncedLength()
{
    sqlite3_stmt *fetch = NULL;
    int res = sqlite3_prepare_v2(_db, "select COUNT(id) from drivesync_synced", -1, &fetch, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Failed to prepare select for fetch syncedLength";
        sqlite3_finalize(fetch);
        return 0;
    }

    if (SQLITE_ROW != sqlite3_step(fetch)) {
        qWarning() << "Error retrieving row " << sqlite3_errmsg(_db);
        sqlite3_finalize(fetch);
        return 0;
    }

    int count = sqlite3_column_int64(fetch, 0);

    sqlite3_finalize(fetch);

    return count;
}

void DriveSyncDatabase::deleteSyncedItem(const SyncedItem &item)
{
    sqlite3_stmt *_dSyncedItem = NULL;

    int res;

    res = sqlite3_prepare_v2(_db, "delete from drivesync_synced where id=?", -1, &_dSyncedItem, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Error preparing deletion: " << sqlite3_errmsg(_db);
        close();
        return;
    }

    res = sqlite3_bind_int64(_dSyncedItem, 1, item.id);
    if (SQLITE_OK != res) {
        qWarning() << "Cannot bind drivesync_synced id for delete";
        sqlite3_finalize(_dSyncedItem);
        return;
    }

    res = sqlite3_step(_dSyncedItem);
    if (SQLITE_DONE != res) {
        qWarning() << "Error executing delete by id in drivesync_synced " << sqlite3_errmsg(_db);
    }

    sqlite3_finalize(_dSyncedItem);
}

SyncedItemList DriveSyncDatabase::getSynced(const int offset, const int limit)
{
    SyncedItemList items;

    sqlite3_stmt *fetch = NULL;
    int res = sqlite3_prepare_v2(_db, "select id, file, checksum from drivesync_synced order by id limit ?1,?2", -1, &fetch, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Failed to prepare select for fetch getSynced";
        sqlite3_finalize(fetch);
        return items;
    }

    res = sqlite3_bind_int64(fetch, 1, offset);
    if (SQLITE_OK != res) {
        qWarning() << "Error on binding offset " << sqlite3_errmsg(_db);
        sqlite3_finalize(fetch);
        return items;
    }

    res = sqlite3_bind_int64(fetch, 2, limit);
    if (SQLITE_OK != res) {
        qWarning() << "Error on binding limit " << sqlite3_errmsg(_db);
        sqlite3_finalize(fetch);
        return items;
    }

    while(SQLITE_ROW == sqlite3_step(fetch))
    {
        SyncedItem item;
        item.id   = sqlite3_column_int64(fetch, 0);
        item.file = DriveSyncDatabase::StringFromUnsignedChar( sqlite3_column_text(fetch, 1) );
        item.checksum = DriveSyncDatabase::StringFromUnsignedChar( sqlite3_column_text(fetch, 2) );
        items.append(item);
    }

    sqlite3_finalize(fetch);

    return items;
}

void DriveSyncDatabase::unShift(const SyncItem syncItem)
{
    this->addToQueue(syncItem.file, syncItem.source);
}

SyncItem DriveSyncDatabase::shift() {
    SyncItem item;

    sqlite3_stmt *fetch = NULL;
    int res = sqlite3_prepare_v2(_db, "select id, file, source from drivesync_queue order by id limit 1", -1, &fetch, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Failed to prepare select for fetch";
        sqlite3_finalize(fetch);
        return item;
    }

    if (SQLITE_ROW != sqlite3_step(fetch)) {
        qWarning() << "Error retrieving row " << sqlite3_errmsg(_db);
        sqlite3_finalize(fetch);
        return item;
    }

    long id = sqlite3_column_int64(fetch, 0);
    QString file   = DriveSyncDatabase::StringFromUnsignedChar( sqlite3_column_text(fetch, 1) );
    QString source = DriveSyncDatabase::StringFromUnsignedChar( sqlite3_column_text(fetch, 2) );

    sqlite3_finalize(fetch);

    res = sqlite3_bind_int64(_delete, 1, id);
    if (SQLITE_OK != res) {
        qWarning() << "Error on Delete binding id " << id << ": " << sqlite3_errmsg(_db);
        return item;
    }

    res = sqlite3_step(_delete);
    if (SQLITE_DONE != res) {
        qWarning() << "Error executing delete " << id << ": " << sqlite3_errmsg(_db);
    }

    sqlite3_reset(_delete);

    item.file = file;
    item.source = source;

    return item;
}

void DriveSyncDatabase::close() {
    if (NULL!=_db) {
        sqlite3_finalize(_insert);
        sqlite3_finalize(_delete);
        sqlite3_close_v2(_db);
    }

    qDebug() << "Database closed";
}

void DriveSyncDatabase::openDatabase(const QString &path) {
    int res = sqlite3_initialize();
    if (SQLITE_OK != res) {
        qWarning() << "ERROR on sqlite3_initialize:" << res;
        return;
    }

    qDebug() << "Opening database " << path;
    res = sqlite3_open_v2(
            path.toAscii(),
            &_db,
            SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX,
            NULL);

    if (SQLITE_OK != res) {
        qWarning() << "Error opening database " << path << ": " << sqlite3_errmsg(_db);
        close();
        return;
    }

    res = sqlite3_exec(_db, "create table if not exists drivesync_queue (id integer primary key not null, file varchar(8192) not null, source varchar(8192) not null)", NULL, NULL, NULL);
    if (SQLITE_OK!=res) {
        qWarning() << "Error creating table drivesync_queue: " << sqlite3_errmsg(_db);
        close();
        return;
    }

    res = sqlite3_prepare_v2(_db, "insert into drivesync_queue(file, source) values (?1, ?2)", -1, &_insert, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Error preparing insertion: " << sqlite3_errmsg(_db);
        close();
        return;
    }

    res = sqlite3_prepare_v2(_db, "delete from drivesync_queue where id=?", -1, &_delete, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Error preparing deletion: " << sqlite3_errmsg(_db);
        close();
        return;
    }

    res = sqlite3_exec(_db, "create table if not exists drivesync_synced (id integer primary key not null, file varchar(8192) not null, checksum varchar(255) not null)", NULL, NULL, NULL);
    if (SQLITE_OK!=res) {
        qWarning() << "Error creating table drivesync_synced: " << sqlite3_errmsg(_db);
        close();
        return;
    }

    qDebug() << "Successfully opened database " << path;
}

bool DriveSyncDatabase::isFileSynced(const QString &path, const QString &hash)
{
    sqlite3_stmt *fetch = NULL;
    int res = sqlite3_prepare_v2(_db, "select id, file from drivesync_synced where file = ?1 and checksum = ?2", -1, &fetch, NULL);
    if (SQLITE_OK != res) {
        qWarning() << "Failed to prepare select for fetch";
        sqlite3_finalize(fetch);
        return NULL;
    }

    res = sqlite3_bind_text(fetch, 1, path.toUtf8().constData(), -1, SQLITE_TRANSIENT);
    if (SQLITE_OK != res) {
        qWarning() << "Error on binding file " << path << ": " << sqlite3_errmsg(_db);
        sqlite3_finalize(fetch);
        return NULL;
    }

    res = sqlite3_bind_text(fetch, 2, hash.toUtf8().constData(), -1, SQLITE_TRANSIENT);
    if (SQLITE_OK != res) {
        qWarning() << "Error on binding file " << path << ": " << sqlite3_errmsg(_db);
        sqlite3_finalize(fetch);
        return NULL;
    }

    if (SQLITE_ROW != sqlite3_step(fetch)) {
        qWarning() << "Error retrieving row " << sqlite3_errmsg(_db);
        sqlite3_finalize(fetch);
        return NULL;
    }

    bool result = false;
    if (sqlite3_data_count(fetch) > 0) {
        result = true;
    } else {
        result = false;
    }

    sqlite3_finalize(fetch);

    return result;
}
