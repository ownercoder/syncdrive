/*
 * GoogleDriveUploader.cpp
 *
 *  Created on: 13 июня 2015 г.
 *      Author: Sergey
 */
#include <QHttpMultiPart>
#include <QHttpPart>
#include <QFile>
#include <QFileInfo>
#include <QSslError>
#include <QTimer>
#include <bb/data/JsonDataAccess>

#include "GoogleDriveUploader.hpp"
#include "SyncItem.hpp"

GoogleDriveUploader::GoogleDriveUploader(QObject * parent) :
    QObject(parent)
{
    totalBytesForSend = 0;
    totalBytesSent = 0;
}

void GoogleDriveUploader::setSessionManager(SessionManager * sessionManager)
{
    sessManager = sessionManager;
}

GoogleDriveUploader::~GoogleDriveUploader()
{

}

void GoogleDriveUploader::onUploadProgress(qint64 bytesSent, qint64 bytesTotal)
{
    emit uploadProgress(totalBytesSent + bytesSent, totalBytesForSend, "");
}

void GoogleDriveUploader::upload()
{
    exec();
}

void GoogleDriveUploader::exec()
{
    emit uploadStarted();
    if (!_queue.length()) {
        emit uploadCompleted();
        return;
    }

    QPair<SyncItem, QString> item = _queue.dequeue();

    file = new QFile(item.first.file);
    if (!file->open(QIODevice::ReadOnly)) {
        qDebug() << file->errorString();
        emit error(file->errorString(), item.first);
        return;
    }

    QString fileName = QFileInfo(item.first.file).fileName();

    QByteArray fileMetaData;

    QVariantMap jsonData;
    jsonData.insert("title", fileName);
    QVariantMap parents;
    parents.insert("id", item.second);
    QVariantList ids;
    ids.append(parents);
    jsonData.insert("parents", ids);

    bb::data::JsonDataAccess *jda = new bb::data::JsonDataAccess();
    jda->saveToBuffer(jsonData, &fileMetaData);
    free(jda);

    multiPart = new QHttpMultiPart(QHttpMultiPart::RelatedType);

    QHttpPart metaDataPart;
    metaDataPart.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    metaDataPart.setBody(fileMetaData);

    QHttpPart filePart;
    filePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/octet-stream"));
    filePart.setBodyDevice(file);

    multiPart->append(metaDataPart);
    multiPart->append(filePart);

    QUrl url("https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart");
    QNetworkRequest request(url);
    GoogleSession currentSession = sessManager->getCurrentSession();
    request.setRawHeader("Authorization", QString(currentSession.tokenType +  " " + currentSession.token).toUtf8());

    accessManager = new QNetworkAccessManager();
    QNetworkReply *reply = accessManager->post(request, multiPart);
    reply->setProperty("filesize", file->size());
    reply->setProperty("filename", fileName);
    QVariant itemVariant = QVariant::fromValue(item.first);
    reply->setProperty("item", itemVariant);

    connect(accessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onAccessManagerFinished(QNetworkReply*)));

    connect(reply, SIGNAL(uploadProgress(qint64,qint64)), this, SLOT(onUploadProgress(qint64,qint64)));
    connect(reply, SIGNAL(finished()), this, SLOT(onFinished()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(onReplyError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(onSslErrors(QList<QSslError>)));
}

void GoogleDriveUploader::onAccessManagerFinished(QNetworkReply *reply)
{
    Q_UNUSED(reply);

    //manager->deleteLater();
    //multiPart->deleteLater();
    file->close();
    delete file;
}

void GoogleDriveUploader::onSslErrors(const QList<QSslError> & errors)
{
    for(int i = 0; i < errors.length(); i++) {
        qDebug() << "SSL error " << errors.at(i).errorString();
    }
}

void GoogleDriveUploader::onReplyError(QNetworkReply::NetworkError code)
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());
    SyncItem item = qvariant_cast<SyncItem>(reply->property("item"));

    delete reply;
    delete multiPart;
    delete accessManager;

    emit error(QString::number(code), item);
}

void GoogleDriveUploader::onFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());

    if (_queue.length()) {
        upload();
    } else {
        totalBytesForSend = 0;
        totalBytesSent = 0;
        emit uploadCompleted();
    }

    delete reply;
    delete multiPart;
    delete accessManager;
}

void GoogleDriveUploader::addToQueue(const SyncItem& item, const QString &uploadTo)
{
    QPair<SyncItem, QString> queuedItem;
    queuedItem.first = item;
    queuedItem.second = uploadTo;

    _queue.enqueue(queuedItem);
    totalBytesForSend = totalBytesForSend + QFileInfo(item.file).size();
}

void GoogleDriveUploader::abort()
{
    _queue.clear();

    totalBytesForSend = 0;
    totalBytesSent = 0;

    emit uploadCompleted();
}
