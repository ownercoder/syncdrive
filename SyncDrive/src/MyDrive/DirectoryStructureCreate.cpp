/*
 * DirectoryStructureCreate.cpp
 *
 *  Created on: 28 мая 2016 г.
 *      Author: sergey
 */

#include <QDebug>
#include <QTimer>
#include "DirectoryStructureCreate.hpp"
#include "DirectoryStructureCreate.hpp"
#include "GoogleFiles.hpp"
#include <QFileInfo>
#include <QStringList>

DirectoryStructureCreate::DirectoryStructureCreate()
{
    tokenManager = new TokenManager(this);

    apiWrapper = new ApiWrapper(this);
    manager = new SessionManager(this);
    manager->setTokenManager(tokenManager);

    connect(apiWrapper, SIGNAL(folderCreated(QVariantMap)), this, SLOT(onFolderCreated(QVariantMap)));
}

DirectoryStructureCreate::~DirectoryStructureCreate()
{
    delete manager;
    delete apiWrapper;
    delete tokenManager;

    loop.deleteLater();
}

QString DirectoryStructureCreate::createPath(const QString &path)
{
    currentPath = "";
    currentFolderId = "root";

    QStringList fragments = path.split("/");
    for(int i=0;i < fragments.length();i++)
    {
        if (fragments.at(i).length())
            _queue.enqueue(fragments.at(i));
    }

    qDebug() << "Start create path: " << path;

    QTimer timer;
    timer.setInterval(60000);
    timer.setSingleShot(true);

    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    QTimer::singleShot(0, this, SLOT(directoryCreateProcessQueue()));

    loop.exec(QEventLoop::AllEvents|QEventLoop::WaitForMoreEvents);

    qDebug() << "Path created: " << path;
    return currentFolderId;
}

void DirectoryStructureCreate::directoryCreateProcessQueue()
{
    if (_queue.length() == 0)
    {
        loop.quit();
        return;
    }

    QString folder = _queue.dequeue();

    currentPath += "/" + folder;

    DirectoryStructureGet *directoryStructureGet = new DirectoryStructureGet();
    QString folderId = directoryStructureGet->getIdByPath(currentPath);
    delete directoryStructureGet;

    if (folderId.isEmpty())
    {
        apiWrapper->createDirectory(folder, currentFolderId);
        return;
    } else {
        currentFolderId = folderId;
    }

    directoryCreateProcessQueue();
}

void DirectoryStructureCreate::onFolderCreated(const QVariantMap &data)
{
    FileMetaData fileMetaData(data);
    currentFolderId = fileMetaData.id;

    directoryCreateProcessQueue();
}

