/*
 * Settings.hpp
 *
 *  Created on: 18 июня 2016 г.
 *      Author: sergey
 */

#ifndef SETTINGS_HPP_
#define SETTINGS_HPP_

#include <QtCore/QStringList>
#include <QSettings>

class Settings: public QSettings
{
    Q_OBJECT
public:
    Settings(QObject *parent = 0);
    Q_INVOKABLE void groupStart(const QString &value);
    Q_INVOKABLE void saveValue(const QString &key, const QString &value);
    Q_INVOKABLE void saveValue(const QString &key, const bool value);
    Q_INVOKABLE QString getValue(const QString &key);
    Q_INVOKABLE bool getBoolValue(const QString &key);
    Q_INVOKABLE QStringList keys();
    Q_INVOKABLE void groupEnd();
    Q_INVOKABLE void clearGroup(const QString &group);
    virtual ~Settings();

};

#endif /* SETTINGS_HPP_ */
