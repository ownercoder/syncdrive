/*
 * Settings.cpp
 *
 *  Created on: 18 июня 2016 г.
 *      Author: sergey
 */
#include <QtCore/QStringList>
#include "Settings.hpp"

Settings::Settings(QObject *parent) : QSettings(parent)
{

}

void Settings::groupStart(const QString &value)
{
    this->beginGroup(value);
}

void Settings::saveValue(const QString &key, const QString &value)
{
    this->setValue(key, value);
    this->sync();
}

void Settings::saveValue(const QString &key, const bool value)
{
    this->setValue(key, value);
    this->sync();
}

bool Settings::getBoolValue(const QString &key)
{
    return this->value(key).toBool();
}

QString Settings::getValue(const QString &key)
{
    return this->value(key).toString();
}

QStringList Settings::keys()
{
    return this->allKeys();
}

void Settings::clearGroup(const QString &group)
{
    this->remove(group);
    this->sync();
}

void Settings::groupEnd()
{
    this->endGroup();
}

Settings::~Settings()
{
    // TODO Auto-generated destructor stub
}

