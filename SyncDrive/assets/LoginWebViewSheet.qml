import bb.cascades 1.4

Sheet {
    id: webPageSheet
    property alias loginUrl: loginWebView.url
    content: Page {
        titleBar: TitleBar {
            title: qsTr("Access request")
            dismissAction: ActionItem {
                onTriggered: {
                    webPageSheet.close();
                }
                title: qsTr("close")
            }
        
        }
        Container {
            background: Color.White
            ScrollView {
                id: scrollView
                scrollViewProperties {
                    scrollMode: ScrollMode.Vertical
                    pinchToZoomEnabled: true
                }
                WebView {
                    objectName: "loginWebView"
                    id: loginWebView
                    settings.viewport: {
                        "width": "device-width",
                        "initial-scale": 1.0
                    }
                    onLoadProgressChanged: {
                        progressIndicator.value = loadProgress;
                    }
                    onLoadingChanged: {
                        if (loadRequest.status == WebLoadStatus.Started) {
                            progressIndicator.visible = true;
                        }
                        if (loadRequest.status == WebLoadStatus.Succeeded) {
                            progressIndicator.visible = false;
                            progressIndicator.value = 0;
                        }
                    }
                    onMinContentScaleChanged: {
                        // Update the scroll view properties to match the content scale
                        // given by the WebView.
                        scrollView.scrollViewProperties.minContentScale = minContentScale;
                        
                        // Let's show the entire page to start with.
                        scrollView.zoomToPoint(0,0, minContentScale,ScrollAnimation.None)
                    }
                    
                    onMaxContentScaleChanged: {
                        scrollView.scrollViewProperties.maxContentScale = maxContentScale; 
                    }
                    onUrlChanged: {
                        var regExpCode = /https\:\/\/.*\?code\=(.*)/i;
                        if (url.toString().match(regExpCode)) {
                            var code = url.toString().match(regExpCode)[1];
                            console.log("Matched url: " + code);
                            app.processToken(code);
                            loginWebView.stop();
                            webPageSheet.close();
                        }
                    }
                    settings.cookiesEnabled: true
                    settings.webInspectorEnabled: true
                }
            }
            attachedObjects: LayoutUpdateHandler {
                onLayoutFrameChanged: {
                    loginWebView.minHeight = layoutFrame.height;
                    console.log("Height: " + layoutFrame.height);
                }
            }
            layout: StackLayout {

            }
            ProgressIndicator {
                visible: true
                id: progressIndicator
                horizontalAlignment: HorizontalAlignment.Fill
                state: ProgressIndicatorState.Progress
                toValue: 100.0
                value: 0.0
                verticalAlignment: VerticalAlignment.Bottom
                implicitLayoutAnimationsEnabled: false
            }
        }
        onCreationCompleted: {
            loginWebView.url = app.getLoginUrl();
        }
    }
}