import bb.cascades 1.4
import com.slytech.settings 1.0

Sheet {
    id: settingsSheet
    content: Page {
        actionBarVisibility: ChromeVisibility.Default
        actionBarAutoHideBehavior: ActionBarAutoHideBehavior.Disabled
        paneProperties: NavigationPaneProperties {
          backButton: ActionItem {
              title: qsTr("Back")
              ActionBar.placement: ActionBarPlacement.OnBar
          
          }
        
        }
        titleBar: TitleBar {
            title: qsTr("Settings")
            acceptAction: ActionItem {
                title: qsTr("Close")
                onTriggered: {
                    settingsSheet.close();
                }
            }

        }
        Container {
          leftPadding: ui.du(2)
          rightPadding: ui.du(2)
          Container {
              layout: StackLayout {
                  orientation: LayoutOrientation.LeftToRight
              }
              topPadding: ui.du(2)
              Label {
                  text: qsTr("Use mobile network")
                  verticalAlignment: VerticalAlignment.Center
                  layoutProperties: StackLayoutProperties {
                      spaceQuota: 1
                  }
              }
              ToggleButton {
                  id: useMobileNetwork
                  horizontalAlignment: HorizontalAlignment.Right
                  checked: settings.getBoolValue("cellularEnabled")
                  onCheckedChanged: {
                      settings.saveValue("cellularEnabled", checked);
                  }
              }
          }
          Label {
              text: qsTr("Additional charges may apply")
              multiline: true
              textStyle.fontSize: FontSize.XSmall
              textStyle.color: Color.DarkGray
          }
          Divider {
          }
          Container {
              layout: StackLayout {
                  orientation: LayoutOrientation.LeftToRight
              }
              Label {
                  text: qsTr("Sync Video")
                  verticalAlignment: VerticalAlignment.Center
                  layoutProperties: StackLayoutProperties {
                      spaceQuota: 1
                  }
              
              }
              ToggleButton {
                  id: syncVideo
                  horizontalAlignment: HorizontalAlignment.Right
                  checked: settings.getBoolValue("syncVideos")
                  onCheckedChanged: {
                      settings.saveValue("syncVideos", checked);
                  }
              }
          }
          Container {
              layout: StackLayout {
                  orientation: LayoutOrientation.LeftToRight
              }
              topPadding: ui.du(2)
              Label {
                  text: qsTr("Sync Photo")
                  verticalAlignment: VerticalAlignment.Center
                  layoutProperties: StackLayoutProperties {
                      spaceQuota: 1
                  }
              
              }
              ToggleButton {
                  id: syncPhoto
                  horizontalAlignment: HorizontalAlignment.Right
                  checked: settings.getBoolValue("syncPhotos")
                  onCheckedChanged: {
                      settings.saveValue("syncPhotos", checked);
                  }
              }
          }
        }
        attachedObjects: [
            Settings {
                id: settings
            }
        ]
    }
}