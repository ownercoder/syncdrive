import bb.cascades 1.4
import bb.system 1.2
import bb.cascades.pickers 1.0
import com.slytech.settings 1.0
import "library.js" as JsHelper

Sheet {
    id: syncListSheet
    Page {
        id: rootPage
        property bool preloading : false
        actionBarVisibility: ChromeVisibility.Compact
        ListView {
            id: listView
            dataModel: ArrayDataModel {
                id: arrayDataModel
                onItemAdded: {
                    listView.dataModelUpdated();
                }
                onItemRemoved: {
                    listView.dataModelUpdated();
                }
            }
            listItemComponents: [
                ListItemComponent {
                    type: "folder"
                    content: Container {
                        animations: [
                            ScaleTransition {
                                id: deleteAnimation
                                duration: 500
                                toX: 0
                                toY: 0
                                onEnded: {
                                    itemContainer.ListItem.view.dataModel.removeAt(itemContainer.ListItem.indexPath);
                                    itemContainer.scaleX = 1.0;
                                    itemContainer.scaleY = 1.0;
                                }
                            }
                        ]
                        id: itemContainer
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        leftPadding: ui.sdu(2)
                        rightPadding: ui.sdu(1)
                        Label {
                            text: ListItemData.folderName
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 2
                            }
                            horizontalAlignment: HorizontalAlignment.Left
                            verticalAlignment: VerticalAlignment.Center
                            textStyle.base: mainTextStyle.style
                            textStyle.fontSizeValue: 9
                            attachedObjects: [
                                TextStyleDefinition {
                                    id: mainTextStyle
                                    
                                    rules: [
                                        FontFaceRule {
                                            source: "asset:///font/Lato-Regular.ttf"
                                            fontFamily: "Lato Regular"
                                        }
                                    ]
                                    fontFamily: "Lato Regular"
                                    fontSize: FontSize.PointValue
                                    color: Color.create("#2f2d2d")
                                }
                            ]
                        }
                        ImageButton {
                            defaultImageSource: "asset:///images/ic_delete.png"
                            pressedImageSource: "asset:///images/ic_delete.png"
                            disabledImageSource: "asset:///images/ic_delete.png"
                            onClicked: {
                                deleteAnimation.play();
                            }
                            horizontalAlignment: HorizontalAlignment.Right
                            verticalAlignment: VerticalAlignment.Center
                        }
                    }
                }
            ]
            function itemType(data, indexPath) {
                return "folder";                
            }
            function dataModelUpdated() {
                if (rootPage.preloading) {
                    return;
                }
                settings.clearGroup("Folders");
                settings.groupStart("Folders");
                console.log("Data model: ");
                console.log(listView.dataModel.size());
                for(var i = 0; i < listView.dataModel.size(); i++) {
                    settings.saveValue(listView.dataModel.value(i).folderPath, listView.dataModel.value(i).folderName);
                }
                settings.groupEnd();
            }
        }
        actions: [
           ActionItem {
                ActionBar.placement: ActionBarPlacement.Signature
                imageSource: "asset:///images/ic_add.png"
                title: qsTr("Add")
                onTriggered: {
                    folderPicker.open();
                }
            }
        ]
        attachedObjects: [
            FilePicker {
                id: folderPicker
                sourceRestriction: FilePickerSourceRestriction.LocalOnly
                mode: FilePickerMode.SaverMultiple
                title: qsTr("Choose folder")
                onFileSelected: {
                    var basename = JsHelper.basename(selectedFiles[0], '');
                    var data = {folderName: basename, folderPath: selectedFiles[0]};
                    for(var i = 0; i < listView.dataModel.size();i++) {
                        if (listView.dataModel.value(i).folderPath == selectedFiles[0]) {
                            systemToast.body = qsTr("Folder already in sync list, choose another folder");
                            systemToast.show();
                            return;
                        }
                    }
                    listView.dataModel.append(data);
                }
            },
            SystemToast {
                id: systemToast
                button.enabled: false
                modality: SystemUiModality.Application
            },
            Settings {
                id: settings
            }
        ]
        titleBar: TitleBar {
            title: qsTr("Folders for sync")
            dismissAction: ActionItem {
                title: qsTr("back")
                onTriggered: {
                    syncListSheet.close();
                }
            }

        }
    }
    onOpened: {
        rootPage.preloading = true;
        arrayDataModel.clear();
        settings.groupStart("Folders");
        var keys = settings.keys();
        for( var i = 0; i < keys.length; i++) {
            var data = {folderName: settings.getValue(keys[i]), folderPath: '/' + keys[i]};
            listView.dataModel.append(data);
        }
        settings.groupEnd();
        rootPage.preloading = false;
    }
}