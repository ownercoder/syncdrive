/*
 * Copyright (c) 2013-2015 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import bb.device 1.4
import bb.cascades 1.4
import com.slytech.connectioninfo 1.0
import com.slytech.sessionwrapper 1.0
import my.library 1.0
import "library.js" as JsHelper

Page {
    actionBarVisibility: ChromeVisibility.Compact
    ScrollView {
      id: scrollArea
      scrollViewProperties.scrollMode: ScrollMode.Vertical
      scrollViewProperties.initialScalingMethod: ScalingMethod.AspectFit
        Container {
          layout: DockLayout {
    
          }
          verticalAlignment: VerticalAlignment.Fill
          horizontalAlignment: HorizontalAlignment.Fill
            Container {
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            attachedObjects: [
                ImagePaintDefinition {
                    id: backgroundImage
                    imageSource: "asset:///images/background.amd"
                },
                TextStyleDefinition {
                    id: subtitleTextStyle
                    
                    rules: [
                        FontFaceRule {
                            source: "asset:///font/Lato-Regular.ttf"
                            fontFamily: "Lato Regular"
                        }
                    ]
                    fontFamily: "Lato Regular"
                    fontSize: FontSize.PointValue
                    color: Color.create("#2f2d2d")
                }
            ]
            background: backgroundImage.imagePaint
            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            
            }
            Container {
                WebImageView {
                    id: webImageView
                    horizontalAlignment: HorizontalAlignment.Center
                    preferredWidth: ui.sdu(30)
                    preferredHeight: ui.sdu(30)
                }
                margin.topOffset: ui.sdu(5)
                margin.bottomOffset: ui.sdu(5)
                horizontalAlignment: HorizontalAlignment.Center
            }
            Label {
                id: email
                text: qsTr("")
                textStyle.textAlign: TextAlign.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.base: subtitleTextStyle.style
                textStyle.fontSizeValue: 9
            }
            Label {
                id: title
                text: ""
                textStyle.textAlign: TextAlign.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.base: subtitleTextStyle.style
                textStyle.fontSizeValue: 7
                margin.topOffset: ui.sdu(5)
            }
            Label {
                id: subtitle
                text: qsTr("use %2 from %3").arg("0").arg(0)
                multiline: true
                textStyle.textAlign: TextAlign.Center
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.base: subtitleTextStyle.style
                textStyle.fontSizeValue: 7
            }
          }
          Container {
             id: waitingOverlay
             layout: DockLayout {
    
              }
              background: Color.create("#ff000000")
             opacity: 0.2
              horizontalAlignment: HorizontalAlignment.Fill
             verticalAlignment: VerticalAlignment.Fill
              ActivityIndicator {
                  running: true
                  verticalAlignment: VerticalAlignment.Fill
                  horizontalAlignment: HorizontalAlignment.Fill
    
              }
              onVisibleChanged: {
                  labelOverlayInfo.visible = visible;
              }
          }
          Label {
              id: labelOverlayInfo
              text: qsTr("")
              horizontalAlignment: HorizontalAlignment.Center
              verticalAlignment: VerticalAlignment.Top
              textStyle.base: subtitleTextStyle.style
              textStyle.fontSizeValue: 9
              multiline: true
            }
      }
      verticalAlignment: VerticalAlignment.Fill
      horizontalAlignment: HorizontalAlignment.Fill
      attachedObjects: [
          LayoutUpdateHandler {
              id: layoutUpdateHandler
              onLayoutFrameChanged: {
                  scrollArea.preferredHeight = layoutFrame.height;
                  scrollArea.preferredWidth = layoutFrame.width;
                  console.log("Layout " + layoutFrame.width);
              }
          }
      ]
        scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.None
    }
    Menu.definition: [
        MenuDefinition {
            settingsAction: SettingsActionItem {
                title: qsTr("Settings")
                onTriggered: {
                    settingsSheet.open();
                }
            }
            actions: [
                ActionItem {
                    title: qsTr("Exit")
                    onTriggered: {
                        introSheet.open();
                        sessWrapper.exitCurrentSession();
                    }
                    imageSource: "asset:///images/ic_exit.png"
                }
            ]
        }
    ]
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.Signature
            title: qsTr("Folders")
            onTriggered: {
                syncList.open();
            }
            imageSource: "asset:///images/ic_entry.png"
        }
    ]
    attachedObjects: [
        Intro {
            id: introSheet
            objectName: "introSheet"
            onClosed: {
                
            }
        },
        SettingsSheet {
            id: settingsSheet
        },
        SessionWrapper {
            id: sessWrapper
        },
        SyncList {
            id: syncList
        },
        ConnectionInfo {
            id: connectionInfo
            onConnectedChanged: {
                connectionUpdate(connected);
            }
        }
    ]
    onCreationCompleted: {
        if (sessWrapper.authenticated == false) {
            introSheet.open();
        }
        connectionUpdate(connectionInfo.connected);
    }
    function closeIntroView() {
        introSheet.close();
        waitingOverlay.visible = true;
    }
    function updateUserInfo(userInfoData) {
        console.log("User info data: ");
        webImageView.url = userInfoData.picture480;
        webImageView.imageMask = "asset:///images/rounded_mask.png";
        subtitle.text = qsTr("use %2 from %3")
            .arg(JsHelper.humanFileSize(userInfoData.quotaBytesUsed))
            .arg(JsHelper.humanFileSize(userInfoData.quotaBytesTotal));
        
        email.text = userInfoData.emailAddress;
        waitingOverlay.visible = false;
        email.visible = true;
        title.visible = true;
        subtitle.visible = true;
    }
    function connectionUpdate(connected) {
        console.log("Connection updated: " + connected);
        if (connected) {
            labelOverlayInfo.text = "";
            app.updateUserInfo();
        } else {
            email.visible = false;
            title.visible = false;
            subtitle.visible = false;
            labelOverlayInfo.text = qsTr("Waiting for internet connection ...");
            waitingOverlay.visible = true;
        }
    }
    function updateLastSync(time, inProgress) {
        if (inProgress) {
            title.text = qsTr("Sync in progress...");
        } else {
            title.text = qsTr("Last sync: %1").arg(JsHelper.timeParse(time));
        }
        console.log("Last sync javascript " + time + " " + JsHelper.timeParse(time));
    }
}
