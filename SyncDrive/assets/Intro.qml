import bb.cascades 1.4
import bb.device 1.4
Sheet {
    id: introSheet
    objectName: "introSheet"
    property LoginWebViewSheet loginWebView
    content: Page {
        Container {
            layout: GridLayout {
                columnCount: 1
            }
            attachedObjects: [
                LayoutUpdateHandler {
                    id: layoutUpdateHandler
                }
            ]
            ListView {
                id: listViewRoot
                preferredWidth: layoutUpdateHandler.layoutFrame.width
                preferredHeight: layoutUpdateHandler.layoutFrame.height
                dataModel: XmlDataModel {
                    id: xmlDataModel
                    source: "asset:///intro.xml"
                }
                flickMode: FlickMode.None
                listItemComponents: [
                    ListItemComponent {
                        id: itemRoot
                        type: "listItem"
                        Container {
                            id: listItemContainer
                            Container {
                            }
                            ImageView {
                                imageSource: ListItemData.image
                                loadEffect: ImageViewLoadEffect.None
                                verticalAlignment: VerticalAlignment.Bottom
                                horizontalAlignment: HorizontalAlignment.Center
                                scalingMethod: ScalingMethod.AspectFit
                            }
                            Label {
                                text: ListItemData.title
                                verticalAlignment: VerticalAlignment.Top
                                horizontalAlignment: HorizontalAlignment.Center
                                textStyle.fontSize: FontSize.XLarge
                                textStyle.base: introTextStyle.style
                                textStyle.fontSizeValue: 9
    
                            }
                            Label {
                                textFormat: TextFormat.Plain
                                text: ListItemData.subtitle
                                verticalAlignment: VerticalAlignment.Top
                                horizontalAlignment: HorizontalAlignment.Fill
                                textStyle.base: introTextStyle.style
                                textStyle.fontSizeValue: 8
                                textStyle.textAlign: TextAlign.Center
                                textStyle.lineHeight: 0.8
                                multiline: true

                            }
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.RightToLeft
    
                                }
                                verticalAlignment: VerticalAlignment.Bottom
                                horizontalAlignment: HorizontalAlignment.Right
                                Label {
                                    text: qsTr("Swipe")
                                    horizontalAlignment: HorizontalAlignment.Fill
                                    verticalAlignment: VerticalAlignment.Bottom
                                    textStyle.textAlign: TextAlign.Right
                                    margin.bottomOffset: ui.du(3)
                                    textStyle.base: introTextStyle.style
                                    textStyle.fontSizeValue: 8
                                }
                                ImageView {
                                    imageSource: "asset:///images/ic_back.png"
                                    loadEffect: ImageViewLoadEffect.None
                                    horizontalAlignment: HorizontalAlignment.Right
                                    verticalAlignment: VerticalAlignment.Bottom
                                    preferredHeight: ui.du(5.4)
                                    scalingMethod: ScalingMethod.AspectFit
                                    margin.bottomOffset: ui.du(2.6)
                                }
                                rightPadding: ui.du(8 * ui.dduFactor)
                            }
                            background: Color.create(ListItemData.color)
                            preferredHeight: listItemContainer.ListItem.view.preferredHeight
                            preferredWidth: listItemContainer.ListItem.view.preferredWidth
                            attachedObjects: [
                                TextStyleDefinition {
                                    id: introTextStyle
                                    
                                    rules: [
                                        FontFaceRule {
                                            source: "asset:///font/Lato-Regular.ttf"
                                            fontFamily: "Lato Regular"
                                        }
                                    ]
                                    fontFamily: "Lato Regular"
                                    fontSize: FontSize.PointValue
                                    color: Color.create("#FFFFFF")
                                }
                            ]
                            layout: GridLayout {
                                columnCount: 1
    
                            }
                        }
                    },
                    ListItemComponent {
                        type: "loginItem"
                        Container {
                            id: loginItemContainer
                            layout: GridLayout {
                                columnCount: 1
                            
                            }
                            Container {
                            }
                            ImageView {
                                imageSource: ListItemData.image
                                loadEffect: ImageViewLoadEffect.None
                                verticalAlignment: VerticalAlignment.Bottom
                                horizontalAlignment: HorizontalAlignment.Center
                                scalingMethod: ScalingMethod.AspectFit
                            }
                            Label {
                                text: ListItemData.title
                                verticalAlignment: VerticalAlignment.Top
                                horizontalAlignment: HorizontalAlignment.Center
                                textStyle.fontSize: FontSize.XLarge
                                textStyle.base: loginTextStyle.style
                                textStyle.fontSizeValue: 9
                            
                            }
                            Label {
                                text: ListItemData.subtitle
                                verticalAlignment: VerticalAlignment.Fill
                                horizontalAlignment: HorizontalAlignment.Fill
                                textStyle.base: loginTextStyle.style
                                textStyle.fontSizeValue: 8
                                textFormat: TextFormat.Html
                                multiline: true
                                textStyle.textAlign: TextAlign.Center
                                textStyle.lineHeight: 0.8
                                margin.bottomOffset: ui.du(10);
                            }
                            ImageButton {
                                defaultImageSource: "asset:///images/GO!.png"
                                pressedImageSource: "asset:///images/GO!.png"
                                disabledImageSource: "asset:///images/GO!.png"
                                horizontalAlignment: HorizontalAlignment.Center
                                verticalAlignment: VerticalAlignment.Top
                                onClicked: {
                                    loginItemContainer.ListItem.view.showLoginWebView();
                                }
                            }
                            bottomPadding: ui.du(4)
                            background: Color.create(ListItemData.color)
                            preferredHeight: loginItemContainer.ListItem.view.preferredHeight
                            preferredWidth: loginItemContainer.ListItem.view.preferredWidth
                            attachedObjects: [
                                TextStyleDefinition {
                                    id: loginTextStyle
                                    
                                    rules: [
                                        FontFaceRule {
                                            source: "asset:///font/Lato-Regular.ttf"
                                            fontFamily: "Lato Regular"
                                        }
                                    ]
                                    fontFamily: "Lato Regular"
                                    fontSize: FontSize.PointValue
                                    color: Color.create("#FFFFFF")
                                }
                            ]                        
                        }
                    }
                ]
                function itemType(data, indexPath) {
                    return data.type;
                }
                scrollIndicatorMode: ScrollIndicatorMode.None
                layout: FlowListLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                snapMode: SnapMode.LeadingEdge
                
                function showLoginWebView(loginWebView) {
                    app.showLoginWebView();
                }
            }
        }
    }
}