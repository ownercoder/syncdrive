APP_NAME = SyncDrive

CONFIG += qt warn_on cascades10

INCLUDEPATH += src/MyDrive

QMAKE_CFLAGS_RELEASE += -fPIC
QMAKE_CXXFLAGS_RELEASE += -fPIC
QMAKE_LFLAGS_RELEASE += -Wl,-z,relro -pie

QT += core network gui

LIBS += -lbbdata -lbbdevice -lbbsystem -lbbcascadespickers -lbb -lbbplatform -lbbutility

include(config.pri)
