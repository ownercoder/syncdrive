<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>Intro</name>
    <message>
        <location filename="../assets/Intro.qml" line="70"/>
        <source>Swipe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginWebViewSheet</name>
    <message>
        <location filename="../assets/LoginWebViewSheet.qml" line="8"/>
        <source>Access request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/LoginWebViewSheet.qml" line="13"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsSheet</name>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="11"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="18"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="20"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="36"/>
        <source>Use mobile network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="52"/>
        <source>Additional charges may apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="64"/>
        <source>Sync Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SettingsSheet.qml" line="86"/>
        <source>Sync Photo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SyncList</name>
    <message>
        <location filename="../assets/SyncList.qml" line="106"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SyncList.qml" line="117"/>
        <source>Choose folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SyncList.qml" line="123"/>
        <source>Folder already in sync list, choose another folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SyncList.qml" line="141"/>
        <source>Folders for sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/SyncList.qml" line="143"/>
        <source>back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>library</name>
    <message>
        <location filename="../assets/library.js" line="6"/>
        <source>HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="9"/>
        <source>Plain text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="12"/>
        <source>Rich text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="15"/>
        <source>Open Office doc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="18"/>
        <source>PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="21"/>
        <source>MS Word document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="24"/>
        <source>MS Excel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="27"/>
        <source>Open Office sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="30"/>
        <source>JPEG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="33"/>
        <source>PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="36"/>
        <source>SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="39"/>
        <source>MS PowerPoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="42"/>
        <source>JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="45"/>
        <source>CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="60"/>
        <source>kB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="60"/>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="60"/>
        <source>GB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="60"/>
        <source>TB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="60"/>
        <source>PB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="60"/>
        <source>EB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="60"/>
        <source>ZB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="60"/>
        <source>YB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="95"/>
        <source>%1 day(s) ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="98"/>
        <source>%1 hour(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="101"/>
        <source>%1 min ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/library.js" line="104"/>
        <source>moment ago</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="153"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="230"/>
        <source>Waiting for internet connection ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="238"/>
        <source>Last sync: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="92"/>
        <location filename="../assets/main.qml" line="211"/>
        <source>use %2 from %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="236"/>
        <source>Sync in progress...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="146"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="64"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="166"/>
        <source>Folders</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
